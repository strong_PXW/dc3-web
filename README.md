 :boom: [IOT DC3](https://gitee.com/pnoker/iot-dc3) Web UI, 项目已正式迁移到 Gitee 上，gitee clone url ： https://gitee.com/pnoker/dc3-web.git

 :seedling: 你的点赞是我们开发的动力！

<p align="center">
    <img src="./dc3/images/iot-dc3-logo.png" width="400"><br>
    <a href='https://gitee.com/pnoker/iot-dc3/stargazers'><img src='https://gitee.com/pnoker/iot-dc3/badge/star.svg?theme=gray' alt='star'></a>
	<a href="https://github.com/pnoker/iot-dc3/blob/master/LICENSE"><img src="https://img.shields.io/github/license/pnoker/iot-dc3.svg"></a>	
	<br><strong>DC3是基于Spring Cloud的开源可分布式物联网(IOT)平台,用于快速开发、部署物联设备接入项目,是一整套物联系统解决方案。<br>IOT DC3 is an open source, distributed Internet of Things (IOT) platform based on Spring Cloud. It is used for rapid development of IOT projects and management of IOT devices. It is a set of solutions for IOT system.</strong>
</p>

------

![](./dc3/images/web-all.png)

### Run & Build 

```bash
git clone https://gitee.com/pnoker/dc3-web.git
cd dc3-web
npm install

# run
npm run serve

# build 
npm run build

# docker build
cd dc3
docker-compose build

# docker run 
docker-compose up -d
```
